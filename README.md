For creating Ecommerce json datastore, I've considered 5 main attributes of ecommerce system. They're product, user, order, seller and invoices.
I've observed 3 products namely "Clapton Chronicles", "One Plus 6 64 GB" and "One Plus 6 128 GB" from "Amazon" website for creating datastore.
I've assigned unique Identification no. (UIN) to each product,seller,user etc. so that I can use this UIN for linking different entities like 
"USR001 user has ordered P001 product and orderID is ORD001, it's seller is SL001". These relationships and constraints must be defined and used 
in application logic to link different javascript objects.I also observed how some attributes are common for Product having subvariants
(One Plus 6- 64GB and 128GB) and included them in datastore as well. The products having variant will have "hasvariant" attribute as true and then
we can iterate over "variant" array of product objects.The attributes of an Audio Album are different than that of a Mobile and can be seen in product 
objects too.(i.e. the schema changes according to type of product and is not fixed)